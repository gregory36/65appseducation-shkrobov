//
//  ViewController.swift
//  65AppsEducation-ProcessingUserEvents
//
//  Created by Гриша Шкробов on 06.11.2021.
//

import UIKit
    
class ViewController: UIViewController {
    
    let viewDobleTap = UIView()
    let viewLongTap = UIView()
    let viewRotation = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewDobleTap.backgroundColor = .red
        viewDobleTap.frame.size = .init(width: 200, height: 200)
        viewDobleTap.center = self.view.convert(CGPoint(x: 200, y: 700), from: view)
        viewDobleTap.layer.cornerRadius = 5
        viewDobleTap.layer.masksToBounds = true
        
        viewRotation.backgroundColor = .yellow
        viewRotation.frame.size = .init(width: 200, height: 150)
        viewRotation.center = self.view.convert(CGPoint(x: 200, y: 150), from: view)
        viewRotation.layer.cornerRadius = 5
        viewRotation.layer.masksToBounds = true
        
        viewLongTap.backgroundColor = .green
        viewLongTap.frame.size = .init(width: 100, height: 200)
        viewLongTap.center = self.view.center
        viewLongTap.layer.cornerRadius = 5
        viewLongTap.layer.masksToBounds = true
        
        
        self.view.addSubview(viewLongTap)
        self.view.addSubview(viewDobleTap)
        self.view.addSubview(viewRotation)
        
        let longTapGR = UILongPressGestureRecognizer(target: self, action: #selector(movingView))
        longTapGR.minimumPressDuration = 0.3
        viewLongTap.addGestureRecognizer(longTapGR)
        
        let doubleTapGR = UITapGestureRecognizer(target: self, action: #selector(changeColor))
        doubleTapGR.numberOfTapsRequired = 2
        viewDobleTap.addGestureRecognizer(doubleTapGR)
        
        let rotationGR = UIRotationGestureRecognizer(target: self, action: #selector(rotateView))
        
        viewRotation.addGestureRecognizer(rotationGR)
    }
    
    @objc private func rotateView(_ recognizer: UIRotationGestureRecognizer){
        switch recognizer.state{
        case .began:
            print("Начало вращения")
        case .changed:
            print("Изменения")
            guard let gestureView = recognizer.view else {
              return
            }

            gestureView.transform = gestureView.transform.rotated(
              by: recognizer.rotation
            )
            recognizer.rotation = 0
        case .ended:
            print("Конец вращения")
        default:
            break
        }
        
        
    }
    
    @objc private func movingView(_ recognizer: UIPanGestureRecognizer){
        let panView = recognizer.view
        switch recognizer.state{
        case .began:
            print("Начало удержания")
            viewLongTap.backgroundColor = .yellow
            let location = recognizer.location(in: view.superview)
            panView?.center.y = location.y
            panView?.center.x = location.x
        case .ended:
            print("Конец удержания")
            viewLongTap.backgroundColor = .green
        case .changed:
            print("Перемещение")
            let location = recognizer.location(in: view.superview)
            panView?.center.y = location.y
            panView?.center.x = location.x
            /*let translation = recognizer.translation(in: view)
              guard let gestureView = recognizer.view else {
                return
              }
              gestureView.center = CGPoint(
                x: gestureView.center.x + translation.x,
                y: gestureView.center.y + translation.y
              )
            recognizer.setTranslation(.zero, in: view)*/
        default:
            break
        }
       
    }
    
    
    @objc
    private func changeColor(_ recognizer: UITapGestureRecognizer){
        //guard recognizer.view != nil else {return}
        
        if viewDobleTap.backgroundColor == .red{
            viewDobleTap.backgroundColor = .blue
        }
        else{
            viewDobleTap.backgroundColor = .red
        }
        
    }


}

